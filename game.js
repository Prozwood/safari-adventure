//EXTERNAL LIBRARIES
var random = require('random')
var stringcase = require('stringcase')
var fs = require('fs')
var soundsJSON = JSON.parse(fs.readFileSync('sounds.json', 'utf8'))

//IMPORTANT VARIABLES
var totalRounds = 3
var difficulties = ["easy", "medium", "hard", "expert"]
var soundURL = "https://s3-us-west-1.amazonaws.com/safarisounds/"

module.exports.start = function(alexa) {
    //SET REQUIRED PERSISTENT VARIABLES TO LOCAL VARIABLES
    var playerCount = alexa.attributes['playerCount']
    var category = stringcase.toTitle(alexa.attributes['category'])
    var difficulty = alexa.attributes['difficulty']
    var currRound = alexa.attributes['currRound']
    var availableAnimals = alexa.attributes['availableAnimals']
    var animalsUsedThisRound = alexa.attributes['animalsUsedThisRound']
    var currPlayer = alexa.attributes['currPlayer']
    var answer = alexa.attributes['answer']
    var animalSound = alexa.attributes['animalSound']
    var playerPoints = alexa.attributes['playerPoints']
    var numCorrect = alexa.attributes['numCorrect']

    //SPEECH VARIABLE
    var speech = ""

    //IF GAME JUST STARTED
    if (currRound === 0) {
        speech += "<say-as interpret-as='interjection'>wow</say-as>. Look at this wonderful scenery. <break time='500ms'/> "

        if (random.randInt(0, 1) === 0) {
            speech += "Let's go this way and see what we find. <break time='1s'/> "
        } else {
            speech += "Let's go that way and see what we find. <break time='1s'/> "
        }

        currRound = 1
        alexa.attributes['currRound'] = currRound

        currPlayer = 0
        alexa.attributes['currPlayer'] = currPlayer

        for (var i = 0; i < playerCount; i++) {
            playerPoints.push(0)
        }
        alexa.attributes['playerPoints'] = playerPoints

        //POPULATE availableAnimals
        availableAnimals = {}
        for (var i = difficulties.indexOf(difficulty); i >= 0; i--) {
            for (var animal in soundsJSON["Categories"][category][difficulties[i]]) {
                for (var j = 0; j < totalRounds; j++) {
                    availableAnimals[animal + j] = difficulties[i]
                }
            }
        }

        //SAVE availableAnimals FOR PERSISTENCE
        alexa.attributes['availableAnimals'] = availableAnimals
    } else {
        //IF THE USER GOT HERE BY SAYING AN ANIMAL'S NAME
        if (alexa.event.request.intent.slots) {
            var exactAnimalGuess = alexa.event.request.intent.slots.animal.value
            var animalGuess = exactAnimalGuess.toLowerCase()

            answerIndex = animalGuess.indexOf(answer[0].toLowerCase())
            //IF exactAnimalGuess CONTAINS answer
            if (answerIndex !== -1) {
                animalGuess = answer[0].toLowerCase()
            } else if (alexa.event.request.intent.slots.animal.resolutions.resolutionsPerAuthority[0].status.code === "ER_SUCCESS_MATCH") {
                animalGuess = alexa.event.request.intent.slots.animal.resolutions.resolutionsPerAuthority[0].values[0].value.name
            }
        } else {
            speech = `Sorry, I didn't get a response earlier. Your response must be a type of animal. Player ${currPlayer}.
                      Which animal does this sound come from? ` + animalSound

            alexa.response.speak(speech).listen(speech)
            alexa.emit(":responseReady")
        }

        //OPTIONS FOR ALEXA TO SAY WHETHER THE PLAYER GOT THE ANSWER WRONG OR RIGHT
        correctExpressions = ['well done', 'way to go', 'hurray', 'booya']
        incorrectExpressions = ['wah wah', 'phooey', 'bummer', 'darn']

        if (exactAnimalGuess) {
            //IF THE USER SAID THE ACTUAL ANSWER
            if (animalGuess === answer[0].toLowerCase()) {
                if (playerCount === 1) {
                    numCorrect++
                    alexa.attributes['numCorrect'] = numCorrect
                }

                randomNum = random.randInt(0, correctExpressions.length - 1)

                speech += `The correct answer is ${answer[0]}. <say-as interpret-as="interjection">${correctExpressions[randomNum]}</say-as>. You got it correct.<break time="1s"/> `

                points = 1
                for (var i = 0; i < difficulties.length; i++) {
                    if (answer[1] === difficulties[i]) {
                        playerPoints[currPlayer - 1] += points
                    }

                    points++
                }

                if (playerPoints[currPlayer - 1] === 1) {
                    speech += "You have " + playerPoints[currPlayer - 1] + " point. "
                } else {
                    speech += "You have " + playerPoints[currPlayer - 1] + " points. "
                }
            } else {
                randomNum = random.randInt(0, incorrectExpressions.length - 1)
                speech += `The correct answer is ${answer[0]}. <say-as interpret-as="interjection">${incorrectExpressions[randomNum]}</say-as>. You were incorrect.<break time="1s"/> `
            }

            answer = []
            alexa.attributes['answer'] = answer
        }
    }

    currPlayer++

    //WHEN CURRENT PLAYER IS GREATER THAN PEOPLE PLAYING, MOVE TO NEXT ROUND
    if (currPlayer === playerCount + 1) {
        currPlayer = 1

        currRound++
        alexa.attributes['currRound'] = currRound

        animalsUsedThisRound = []
        alexa.attributes['animalsUsedThisRound'] = animalsUsedThisRound
    }

    alexa.attributes['currPlayer'] = currPlayer

    //IF THE CURRENT ROUND IS NOT GREATER THAN THE POSSIBLE ROUNDS
    if (currRound !== totalRounds + 1) {
        var storyStatements = [
            "<say-as interpret-as='interjection'>heads up</say-as>. You don't want to fall out of the truck now, do you? ",
            "<say-as interpret-as='interjection'>no way</say-as>. Did you see that? ",
            "What in the world is that? I've never seen an animal like that before. ",
            "I love the view from here. It's pretty great, isn't it? ",
            "Be careful. You don't want to attract the attention of any angry animals. ",
            "I love those animals. They are so cute. ",
            "I'm having a lot of fun on our safari trail, aren't you? ",
            "Ah, these creatures can be so majestic. It's great. ",
            "Ew. I think a fly just flew into my mouth."
        ]
        randomNum = random.randInt(0, storyStatements.length - 1)
        speech += " <break time='1s'/> " + storyStatements[randomNum] + " <break time='2s'/> "

        var animalKeys = Object.keys(availableAnimals)
        var randomAnimal = animalKeys[random.randInt(0, animalKeys.length - 1)]
        var randomAnimalName = randomAnimal.substring(0, randomAnimal.length - 1)

        while (animalsUsedThisRound.includes(randomAnimalName)) {
            randomAnimal = animalKeys[random.randInt(0, animalKeys.length - 1)]
            randomAnimalName = randomAnimal.substring(0, randomAnimal.length - 1)
        }

        answer.push(randomAnimalName)
        animalsUsedThisRound.push(randomAnimalName)
        alexa.attributes['animalsUsedThisRound'] = animalsUsedThisRound

        var animalDifficulty = availableAnimals[randomAnimal]

        answer.push(animalDifficulty)
        alexa.attributes['answer'] = answer

        var filename = soundsJSON["Categories"][category][animalDifficulty][randomAnimalName]

        animalSound = '<audio src="' + soundURL + filename + '" />'
        alexa.attributes['animalSound'] = animalSound

        alexaExpressions = [`wow`, `ooh la la`]
        randomNum = random.randInt(0, alexaExpressions.length - 1)
        if (playerCount > 1) {
            questions = [
                `Player ${currPlayer}, Which animal does this sound come from? <break time="400ms"/> ${animalSound}`,
                `${animalSound} <break time="400ms"/> <say-as interpret-as="interjection">${alexaExpressions[randomNum]}</say-as>.
                Player ${currPlayer}, Which animal do you think that came from?`,
                `${animalSound} <break time="400ms"/> Player ${currPlayer}, could you help me identify the animal that made that sound?`
            ]
        } else {
            questions = [
                `Which animal does this sound come from? <break time="400ms"/> ${animalSound}`,
                `${animalSound} <break time="400ms"/> <say-as interpret-as="interjection">${alexaExpressions[randomNum]}</say-as>. Which animal do you think that came from?`,
                `${animalSound} <break time="400ms"/> Could you help me identify the animal that made that sound?`
            ]
        }

        randomNum = random.randInt(0, questions.length - 1)
        speech += `${questions[randomNum]}` //Round ${currRound}.

        delete availableAnimals[randomAnimal]
        alexa.attributes['availableAnimals'] = availableAnimals

        alexa.response.speak(speech).listen(speech)
    } else {
        speech += "<say-as interpret-as='interjection'>aw man</say-as>, the safari has come to an end. <break time='800ms'/> "

        if (playerCount > 1) {
            winner = []
            maxPoints = 0
            for (var i = 0; i < playerPoints.length; i++) {
                speech += `Player ${i + 1} got `

                if (playerPoints[i] === 0) {
                    speech += `no points. `
                } else {
                    speech += `${playerPoints[i]} `

                    if (playerPoints[i] === 1) {
                        speech += "point. "
                    } else {
                        speech += "points. "
                    }
                }

                if (playerPoints[i] > maxPoints) {
                    maxPoints = playerPoints[i]
                    winner = [i + 1]
                } else if (playerPoints[i] === maxPoints) {
                    winner.push(i + 1)
                }
            }

            if (winner.length === 1) {
                speech += `<say-as interpret-as='interjection'>yay</say-as>, congratulations player ${winner}. You win. `
            } else {
                speech += `<say-as interpret-as='interjection'>wow</say-as>, players `

                for (var i = 0; i < winner.length; i++) {
                    if (i === winner.length - 1) {
                        speech += `and ${winner[i]} tied for first place. `
                    } else {
                        speech += `${winner[i]}, `
                    }
                }
            }
        } else {
            if (numCorrect === 0) {
                speech += `Unfortunately, you didn't get any of the questions right, so you ended with zero points. `
            } else {
                if (numCorrect === 3) {
                    speech += `You got all of the questions right, earning you ${playerPoints[0]} `
                } else {
                    speech += `You got ${numCorrect} of the questions right, earning you ${playerPoints[0]} `
                }

                if (playerPoints === 1) {
                    speech += "point. "
                } else {
                    speech += "points. "
                }
            }

            maxPoints = playerPoints[i]
        }

        speech += "Thank you for playing. If you'd like to play again, please say <break time='100ms'/>start over."
        alexa.response.speak(speech).listen("If you'd like to play again, please say <break time='100ms'/>start over.")
    }

    alexa.emit(":responseReady")
}
