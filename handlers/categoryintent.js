var random = require('random')

module.exports.intent = function() {
    lastIntent = this.attributes['lastIntent']
    this.attributes['lastIntent'] = "CategoryIntent"

    var playerCount = this.attributes['playerCount']
    var category = this.attributes['category']

    if (lastIntent === "CategoryIntent" || lastIntent === "DescriptorIntent" || lastIntent === "NonDescriptorIntent") {
        var speech = ""

        if (this.event.request.intent.slots) {
            if (this.event.request.intent.slots.category.resolutions.resolutionsPerAuthority[0].status.code === "ER_SUCCESS_MATCH") {
                category = this.event.request.intent.slots.category.resolutions.resolutionsPerAuthority[0].values[0].value.name
            } else {
                category = this.event.request.intent.slots.category.value
            }

            this.attributes['category'] = category
        } else {
            speech = "Which safari did you want to join?"
            this.response.speak(speech).listen("Sorry, I didn't catch that. " + speech)
            this.emit(":responseReady")
        }

        if (random.randInt(0, 2) === 0) {
            speech = "You have selected the safari with " + category + ". "
        } else {
            speech = "You have selected the " + category + " safari. "
        }

        speech += "<break time='300ms'/>Please select one of the four difficulties. Your options are <break time='100ms'/>easy, medium, hard, and expert."

        this.response.speak(speech).listen("Please select a difficulty.")
        this.emit(":responseReady")
    } else if (!this.event.session.new && lastIntent !== "null") {
        this.emit(lastIntent)
    } else {
        this.emit("LaunchRequest")
    }
}
