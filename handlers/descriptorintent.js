var random = require('random')

var errPlayersStatements = [
    "Sorry, I couldn't get that. How many players will be joining us?",
    "I didn't understand. Please repeat the number of players.",
    "Sorry, I didn't catch that. How many players did you say would be playing today?"
]

module.exports.intent = function() {
    var lastIntent = this.attributes['lastIntent']
    this.attributes['lastIntent'] = "DescriptorIntent"

    var playerCount = this.attributes['playerCount']

    if (lastIntent === "DescriptorIntent" || lastIntent === "PlayerIntent" || lastIntent === "NonDescriptorIntent") {
        var speech = ""

        if (playerCount !== 0) {
            speech += `The five categories are as follows.
                       Birds, like Pigeons and Eagles, <prosody rate='fast'>Cats</prosody> and Dogs, such as Lions and Hyenas, Farm Animals, like Cows and Pigs, Water Animals, such as dolphins, and <prosody rate='fast'>Grab</prosody> Bag, which is made up of many different types of animals.
                       If you'd like me to repeat what I just said, please say <break time='100ms'/>go back<break time='100ms'/>, otherwise please say the name of the safari you would like to join.`

            this.response.speak(speech).listen("Which safari did you want to join?")
        } else {
            randomNum = random.randInt(0, errPlayersStatements.length - 1)
            this.response.speak(errPlayersStatements[randomNum]).listen("How many players will be joining me?")
        }

        this.emit(":responseReady")
    } else if (lastIntent === "GameIntent" && this.attributes['currRound'] === 4) {
        this.emit("LaunchRequest")
    } else if (!this.event.session.new && lastIntent !== "null") {
        this.emit(lastIntent)
    } else {
        this.emit("LaunchRequest")
    }
}
