var random = require('random')

var readyStatements = [
    "Are you ready? To begin the safari, please say <break time='100ms'/>begin.",
    "Are you ready to go? To get the adventure started, please say <break time='100ms'/>start the safari.",
    "Are you ready to help me identify animals on the safari? If so, please say <break time='100ms'/>start."
]

module.exports.intent = function() {
    var lastIntent = this.attributes['lastIntent']
    this.attributes['lastIntent'] = "DifficultyIntent"

    var difficulty = this.attributes['difficulty']

    var speech = ""

    if (lastIntent === "DifficultyIntent" || lastIntent === "CategoryIntent") {
        if (this.event.request.intent.slots) {
            difficulty = this.event.request.intent.slots.difficulty.value
            this.attributes['difficulty'] = difficulty
        } else {
            speech = "Which level of difficulty do you want to play on?"
            this.response.speak(speech).listen("Sorry, I didn't catch that. " + speech)
            this.emit(':responseReady')
        }

        speech = "You have selected the " + difficulty + " difficulty. "

        randomNum = random.randInt(0, readyStatements.length - 1)
        speech += readyStatements[randomNum] + "<break time='200ms'/> <say-as interpret-as='interjection'>good luck</say-as>"

        this.response.speak(speech).listen("Please say either <break time='100ms'/>start<break time='100ms'/> or <break time='100ms'/>begin<break time='100ms'/> to get the safari going.")
        this.emit(":responseReady")
    } else if (!this.event.session.new && lastIntent !== "null") {
       this.emit(lastIntent)
   } else {
       this.emit("LaunchRequest")
   }
}
