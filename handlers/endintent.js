var random = require('random')

var partingStatements = ["Goodbye. See you again soon.", "Thank you for riding along with me. Until next time.", "Thank you for playing Safari Adventure. Please come back soon.", "See you again soon."]

module.exports.intent = function() {
    this.attributes['playerCount'] = 0
    this.attributes['category'] = "null"
    this.attributes['lastIntent'] = "PlayerIntent"
    this.attributes['difficulty'] = "null"
    this.attributes['currRound'] = 0
    this.attributes['availableAnimals'] = {}
    this.attributes['currPlayer'] = 0
    this.attributes['answer'] = []
    this.attributes['animalSound'] = "null"
    this.attributes['playerPoints'] = []
    this.attributes['numCorrect'] = 0
    this.attributes['animalsUsedThisRound'] = []

    randomNum = random.randInt(0, partingStatements.length - 1)

    this.response.speak(partingStatements[randomNum])
    this.emit(":responseReady")
}
