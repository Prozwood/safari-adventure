var game = require('game')

module.exports.intent = function() {
    var lastIntent = this.attributes['lastIntent']
    this.attributes['lastIntent'] = "GameIntent"

    var playerCount = this.attributes['playerCount']
    var difficulty = this.attributes['difficulty']

    if (lastIntent === "GameIntent" || lastIntent === "CategoryIntent") {
        if (difficulty !== "null") {
            game.start(this)
        } else if (this.event.request.intent.slots) {
            difficulty = this.event.request.intent.slots.difficulty.value
            this.attributes['difficulty'] = difficulty

            game.start(this)
        } else {
            missingDifficulty(this)
        }
    } else if (lastIntent === "DifficultyIntent") {
        if (difficulty !== "null") {
            game.start(this)
        } else {
            missingDifficulty(this)
        }
    } else if (!this.event.session.new) {
        var availableAnimals = this.attributes['availableAnimals']
        if (Object.keys(availableAnimals).length > 0) {
            game.start(this)
        } else {
            if (lastIntent !== "null") {
                this.emit(lastIntent)
            } else {
                this.emit("LaunchRequest")
            }
        }
    } else {
        this.emit("LaunchRequest")
    }
}

function missingDifficulty(alexa) {
    alexa.response.speak("I didn't get the level of difficulty. Please pick easy, medium, hard, or expert").listen("Sorry, I didn't catch that, please choose a level of difficulty")
    alexa.emit(":responseReady")
}
