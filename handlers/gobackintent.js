module.exports.intent = function() {
    var lastIntent = this.attributes['lastIntent']

    if (lastIntent === "null" || lastIntent === "GameIntent") {
        this.emit("LaunchRequest")
    } else {
        this.emit(lastIntent)
    }
}
