module.exports.intent = function() {
    var lastIntent = this.attributes['lastIntent']

    switch(lastIntent) {
        case "PlayerIntent":
            this.response.speak(`Now that you have decided how many players you want, you must tell the bot whether or not you want to hear about each of the safari categories.
                                 For example, to hear about the categories, you can say <break time='100ms'/>yes please<break time='100ms'/>
                                 or if you already know which safari you'd like to join, you can say<break time='100ms'/> no thanks. If you want to go back, just say <break time='100ms'/>go back.`)
                         .listen("Try saying <break time='100ms'/>yes<break time='100ms'/> or <break time='100ms'/>no.")
            break

        case "DescriptorIntent":
            this.response.speak(`After hearing about each of the safari categories, you must decide which one you would most like to ride along with.
                                 For example, if you want to join the safari with farm animals, simply say <break time='100ms'/>farm animals. If you want to go back, just say <break time='100ms'/>go back.`)
                         .listen("Try saying <break time='100ms'/>farm animals.")
            break

        case "NonDescriptorIntent":
            this.response.speak(`If you don't remember the five categories, I am here to help. There are birds, cats and dogs, farm animals, water animals, and grab bag.
                                 If you need more information on them, please say<break time='100ms'/> tell me about the categories. If you want to go back, just say <break time='100ms'/>go back.`)
                         .listen("Try saying <break time='100ms'/>tell me about the safari options.")
            break

        case "CategoryIntent":
            this.response.speak(`Now that you have selected a category, you must choose a difficulty. The four options are easy, medium, hard, and expert. Each difficulty includes sounds from the difficulties below.
                                 If this is your first time playing, I recommend you try the easy difficulty. Go ahead and say the difficulty you'd like to play, for example,
                                 <break time='100ms'/>easy.<break time='100ms'/> If you want to go back, just say <break time='100ms'/>go back.`)
                         .listen("Try saying<break time='100ms'/> easy difficulty please.")
            break

        case "DifficultyIntent":
            this.response.speak(`Now that you have selected a difficulty, you must confirm that you are ready for the safari to begin. If you aren't ready you will have to restart the skill.
                                 If you are ready, then just say <break time='100ms'/>begin<break time='100ms'/> or <break time='100ms'/>start. If you want to go back, just say <break time='100ms'/>go back.`)
                         .listen("Try saying<break time='100ms'/> begin the safari.")
            break

        case "GameIntent":
            this.response.speak(`Once the safari begins, it will prompt players to identify animal sounds. It is possible to get the same sound multiple times per game which means you'll have to put your memory to the test.
                                 Either before or after the animal sound is played, I will ask you which animal you think makes that sound. If you get it wrong you won't get any points, but if you get it right, you get points based on the difficulty of the question.
                                 If you are answering your second question, and it happens to be a hard one, for example, you have the opportunity to earn three points.
                                 To get back to the safari, please say <break time='100ms'/>go back.`)
                         .listen("Try saying <break time='100ms'/>go back.")
            break

        default:
            this.response.speak(`Safari Adventure is a skill all about teaching the wonderous sounds of the animals around us to everyone, from children to adults.
                                 To aid in the practicality of teaching simple and hard animal noises, there are separate safari categories and difficulties.
                                 All you need to do here is tell me how many people want to join me on the safari adventure. The safari can have one to four people on it.
                                 For example, if you want to play with a single friend or family member, just say <break time='100ms'/>two players. Give it a shot.`)
                         .listen("Try saying a number between one and four.")
            break
    }

    this.emit(':responseReady')
}
