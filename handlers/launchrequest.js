var random = require('random')

var numPlayersStatements = [
    "How many players will be riding with me today?",
    "How many players want to join me on the safari?"
]
var nothingEnteredStatements = [
    "Sorry, I didn't hear anything. Please tell me how many people will be joining me on the safari.",
    "I didn't get that. Could you please say the number of players competing?"
]

module.exports.request = function() {
    this.attributes['playerCount'] = 0
    this.attributes['category'] = "null"
    this.attributes['lastIntent'] = "null"
    this.attributes['difficulty'] = "null"
    this.attributes['currRound'] = 0
    this.attributes['availableAnimals'] = {}
    this.attributes['currPlayer'] = 0
    this.attributes['answer'] = []
    this.attributes['animalSound'] = "null"
    this.attributes['playerPoints'] = []
    this.attributes['numCorrect'] = 0
    this.attributes['animalsUsedThisRound'] = []

    descriptionStatements = [
        "A game all about animal sounds.",
        "A game about learning animal sounds.",
        "An educational game about animal sounds."
    ]
    randomNum = random.randInt(0, descriptionStatements.length - 1)

    var speech = `Welcome to Safari Adventure. <break time="100ms"/>${descriptionStatements[randomNum]}<break time="400ms"/> `

    if (random.randInt(0, 2) === 1) {
        speech += '<say-as interpret-as="interjection">boom</say-as>. <break time="400ms"/> '
    }

    randomNum = random.randInt(0, numPlayersStatements.length - 1)
    speech += numPlayersStatements[randomNum]

    randomNum = random.randInt(0, nothingEnteredStatements.length - 1)
    var question = nothingEnteredStatements[randomNum]

    this.response.speak(speech).listen(question)
    this.emit(':responseReady')
}
