var random = require('random')

var errPlayersStatements = [
    "Sorry, I couldn't get that. How many players will be joining us?",
    "I didn't understand. Please repeat the number of players.",
    "Sorry, I didn't catch that. How many players did you say would be playing today?"
]

module.exports.intent = function() {
    lastIntent = this.attributes['lastIntent']
    this.attributes['lastIntent'] = "NonDescriptorIntent"

    var playerCount = this.attributes['playerCount']
    var category = this.attributes['category']

    if (lastIntent === "NonDescriptorIntent" || lastIntent === "PlayerIntent" || lastIntent === "CategoryIntent" || lastIntent === "DescriptorIntent") {
        var speech = ""

        if (random.randInt(0, 1) === 0) {
            speech = "Which category would you like for your safari?"
        } else {
            speech = "Please pick one of the five safari options."
        }

        this.response.speak(speech).listen("The categories are birds, cats and dogs, farm animals, sea creatures, and grab bag. Please say the name of the safari you want to join.")
        this.emit(":responseReady")
    } else if (lastIntent === "GameIntent" && this.attributes['currRound'] === 4) {
        this.emit("AMAZON.StopIntent")
    } else if (!this.event.session.new && lastIntent !== "null") {
        this.emit(lastIntent)
    } else {
        this.emit("LaunchRequest")
    }
}
