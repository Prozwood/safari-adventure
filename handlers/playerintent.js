var random = require('random')

var tooManyPlayersStatements = ["uh oh", "oof", "oh brother", "mamma mia", "d'oh"]

module.exports.intent = function() {
    var lastIntent = this.attributes['lastIntent']

    var speech = ""

    if (this.event.session.new || lastIntent === "null" || lastIntent === "PlayerIntent") {
        this.attributes['playerCount'] = 0
        this.attributes['category'] = "null"
        this.attributes['lastIntent'] = "PlayerIntent"
        this.attributes['difficulty'] = "null"
        this.attributes['currRound'] = 0
        this.attributes['availableAnimals'] = {}
        this.attributes['currPlayer'] = 0
        this.attributes['answer'] = []
        this.attributes['animalSound'] = "null"
        this.attributes['playerPoints'] = []
        this.attributes['numCorrect'] = 0
        this.attributes['animalsUsedThisRound'] = []

        if (this.event.request.intent.slots) {
            playerCount = this.event.request.intent.slots.playernum.value

            if (playerCount >= 1 && playerCount <= 4) {
                this.attributes['playerCount'] = parseInt(playerCount)

                if (playerCount !== "1") {
                    speech += "Ok, " + playerCount + " players it is. <break time='300ms'/> "
                } else {
                    speech += "Ok, " + playerCount + " player it is. <break time='300ms'/> "
                }

                speech += "Now let's select the safari you'd like to join. There are five different safaris. Would you like to hear about each of them?"

                this.response.speak(speech).listen("I didn't hear anything. Do you want to hear about the safaris?")
            } else {
                randomNum = random.randInt(0, tooManyPlayersStatements.length - 1)
                speech += '<say-as interpret-as="interjection">' + tooManyPlayersStatements[randomNum] + '</say-as> <break time="200ms"/> '

                if (playerCount > 4) {
                    speech += "The safari can hold up to four people. "
                }

                speech += "Please pick a number one through four."

                this.response.speak(speech).listen("Sorry, I didn't catch that, please pick a number one through four")
            }
        } else {
            speech = "Please tell me the number of people joining me on the safari."
            this.response.speak(speech).listen("Sorry, I didn't catch that. " + speech)
        }

        this.emit(':responseReady')
    } else if (lastIntent === "GameIntent" && this.attributes['currRound'] === 4) {
        this.emit("LaunchRequest")
    } else {
        this.emit(lastIntent)
    }
}
