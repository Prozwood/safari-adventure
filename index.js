//EXTERNAL LIBRARY
const Alexa = require('alexa-sdk')

//DEFINE HANDLERS
var path = "handlers/"

var launch = require(path + 'launchrequest')
var player = require(path + 'playerintent')
var descriptor = require(path + 'descriptorintent')
var nondescriptor = require(path + 'nondescriptorintent')
var category = require(path + 'categoryintent')
var difficulty = require(path + 'difficultyintent')
var game = require(path + 'gameintent')
var goback = require(path + 'gobackintent')
var end = require(path + 'endintent')
var help = require(path + 'helpintent')
var unhandled = require(path + 'unhandledintent')

//SET UP ALEXA-SDK
exports.handler = function(event, context, callback) {
    const alexa = Alexa.handler(event, context, callback)
    alexa.APP_ID = "amzn1.ask.skill.5cfa30d1-5604-4c0d-bfd9-d2581c843d4c"
    alexa.dynamoDBTableName = "Safari"
    alexa.registerHandlers(handlers)
    alexa.execute()
}

//ASSIGN HANDLERS
const handlers = {
    //CUSTOM INTENTS
    'LaunchRequest': launch.request,
    'PlayerIntent': player.intent,
    'DescriptorIntent': descriptor.intent,
    'NonDescriptorIntent': nondescriptor.intent,
    'CategoryIntent': category.intent,
    'DifficultyIntent': difficulty.intent,
    'GameIntent': game.intent,
    'GoBackIntent': goback.intent,

    //REQUIRED INTENTS
    'AMAZON.CancelIntent': end.intent,
    'AMAZON.HelpIntent': help.intent,
    'AMAZON.StopIntent': end.intent,

    //UNHANDLED EVENT
    'Unhandled': unhandled.intent
}
